-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: quizcake
-- ------------------------------------------------------
-- Server version	5.5.28-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer_students`
--

DROP TABLE IF EXISTS `answer_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `answer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_as_student_id_idx` (`student_id`),
  KEY `fk_as_answer_id_idx` (`answer_id`),
  CONSTRAINT `fk_as_answer_id` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_as_student_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_students`
--

LOCK TABLES `answer_students` WRITE;
/*!40000 ALTER TABLE `answer_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `name` char(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ans_idx` (`question_id`),
  CONSTRAINT `fk_ans_question_id` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (3,1,'Franklin D Rosevelt'),(4,1,'Benjamin Franklin'),(5,1,'James Adams'),(6,1,'George Washington'),(7,1,'Barak Obama'),(8,2,'Drakar'),(9,2,'Rupia'),(10,2,'Marco'),(11,2,'Dinar'),(12,2,'Dolar Indio'),(13,3,'Brasil'),(14,3,'Colombia'),(15,3,'Uruguay'),(16,3,'Bolivia'),(17,3,'PanamÃ¡'),(18,4,'BerlÃ­n'),(19,4,'Estocolmo'),(20,4,'Helsinki'),(21,4,'Varsovia'),(22,4,'Paris'),(23,5,'Mario Vargas Llosa'),(24,5,'Julio Verne'),(25,5,'Gabriel García Marquez'),(26,5,'Gabriela Mistral'),(27,5,'Pablo Neruda'),(28,6,'Nueva York'),(29,6,'Albani'),(30,6,'Vermont'),(31,6,'New Jersey'),(32,6,'Manhattan'),(33,7,'Bogotá'),(34,7,'Buenos Aires'),(35,7,'Santiago de Chile'),(36,7,'Ciudad de México'),(37,7,'Sao Paulo'),(38,8,'Junker'),(39,8,'Enola Gay'),(40,8,'Meredith'),(41,8,'Stucka'),(42,8,'Kamikaze'),(43,9,'Queshua'),(44,9,'Chibcha'),(45,9,'Guarani'),(46,9,'Español'),(47,9,'Mapudungun'),(48,10,'Puerto Rico'),(49,10,'Hawai'),(50,10,'Samoa'),(51,10,'Midway'),(52,10,'Cuba'),(53,11,'Hecho acontecido'),(54,11,'Lugar indeterminado'),(55,11,'Actuar de hecho'),(56,11,'En el acto'),(57,11,'Lugar actual'),(58,12,'Tierra'),(59,12,'Venus'),(60,12,'Saturno'),(61,12,'Mercurio'),(62,12,'Neptuno');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `right_answer` int(10) unsigned DEFAULT NULL,
  `points` float NOT NULL,
  `url` text COMMENT 'Esta imagen va a contener la imagen que va al lado de las respuestas de una pregunta el la prueba de modo que el sistema pueda reconocer que pregunta es (prueba, profe, curso, etc)',
  PRIMARY KEY (`id`),
  KEY `fk_que_test_id_idx` (`quiz_id`),
  CONSTRAINT `fk_que_quiz_id` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,1,'Â¿QuiÃ©n fue el primer presidente de los Estados Unidos?',1,4,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1'),(2,1,'Â¿CuÃ¡l es la moneda de la India?',2,2,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2'),(3,1,'Â¿QuÃ© pais de sudamÃ©rica esta rodeado por el ocÃ©ano AtlÃ¡ntico y ocÃ©ano PacÃ­fico?',3,2,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3'),(4,1,'Â¿CuÃ¡l es la capital de Suecia?',4,2,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4'),(5,2,'Qué escritor premio nobel escribió el libro Cien años de soledad?',1,3,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1'),(6,2,'¿Cuál es la capital del estado de Nueva York?',2,2,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2'),(7,2,'¿Qué capital latinoamericana es conocida como la Atenas Suramericana?',3,1,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3'),(8,2,'¿Cómo se llamó el avión que lanzó la bomba atómica sobre Japón?',4,2,3,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4'),(9,3,'Cuál es el dialecto originario de Paraguay?',1,3,5,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1'),(10,3,'¿Cuál es la principal isla de Estados Unidos?',2,2,5,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2'),(11,3,'¿Qué significado se le da en el castellano a la locución latina ipso facto?',3,4,5,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3'),(12,3,'¿Cuál de los siguientes planetas es el mas cercano al Sol?',4,4,5,'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quiz` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `class` char(100) NOT NULL,
  `course` char(100) DEFAULT NULL,
  `test_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tes_teacher_id_idx` (`teacher_id`),
  CONSTRAINT `fk_tes_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
INSERT INTO `quiz` VALUES (1,2,'Cultura y Sociedad','4to medio',1),(2,2,'Cultura y Sociedad','4to medio',2),(3,2,'Cultura y Sociedad','4to medio',3),(4,2,'Cultura y Sociedad','4to Medio A',4);
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `dni` char(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Karen','123'),(2,'Macarena','234'),(3,'Pia','345');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (2,'Julio'),(3,'Gonzalo'),(4,'Pablo');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-11-25 10:57:49
