CREATE SCHEMA IF NOT EXISTS `quizcake` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

create user quizcake identified by 'quizcake';

grant all privileges on quizcake.* to quizcake@localhost identified by 'quizcake';

