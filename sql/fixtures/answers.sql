INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (1, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (1, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (1, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (1, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (1, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (2, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (2, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (2, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (2, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (2, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (3, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (3, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (3, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (3, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (3, 'Quita Respuesta');


INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (4, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (4, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (4, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (4, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (4, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (5, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (5, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (5, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (5, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (5, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (6, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (6, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (6, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (6, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (6, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (7, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (7, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (7, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (7, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (7, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (8, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (8, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (8, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (8, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (8, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (9, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (9, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (9, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (9, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (9, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (10, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (10, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (10, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (10, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (10, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (11, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (11, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (11, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (11, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (11, 'Quita Respuesta');

INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (12, 'Primera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (12, 'Segunda Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (12, 'Tercera Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (12, 'Cuarta Respuesta');
INSERT INTO `quizcake`.`answers` (`question_id`, `name`) VALUES (12, 'Quita Respuesta');



UPDATE `quizcake`.`answers` SET `name`='Franklin D Rosevelt' WHERE `id`='1';
UPDATE `quizcake`.`answers` SET `name`='Benjamin Franklin' WHERE `id`='2';
UPDATE `quizcake`.`answers` SET `name`='James Adams' WHERE `id`='3';
UPDATE `quizcake`.`answers` SET `name`='George Washington' WHERE `id`='4';
UPDATE `quizcake`.`answers` SET `name`='Barak Obama' WHERE `id`='5';

UPDATE `quizcake`.`answers` SET `name`='Drakar' WHERE `id`='6';
UPDATE `quizcake`.`answers` SET `name`='Rupia' WHERE `id`='7';
UPDATE `quizcake`.`answers` SET `name`='Marco' WHERE `id`='8';
UPDATE `quizcake`.`answers` SET `name`='Dinar' WHERE `id`='9';
UPDATE `quizcake`.`answers` SET `name`='Dolar Indio' WHERE `id`='10';

UPDATE `quizcake`.`answers` SET `name`='Brasil' WHERE `id`='11';
UPDATE `quizcake`.`answers` SET `name`='Colombia' WHERE `id`='12';
UPDATE `quizcake`.`answers` SET `name`='Uruguay' WHERE `id`='13';
UPDATE `quizcake`.`answers` SET `name`='Bolivia' WHERE `id`='14';
UPDATE `quizcake`.`answers` SET `name`='Panamá' WHERE `id`='15';

UPDATE `quizcake`.`answers` SET `name`='Berlín' WHERE `id`='16';
UPDATE `quizcake`.`answers` SET `name`='Estocolmo' WHERE `id`='17';
UPDATE `quizcake`.`answers` SET `name`='Helsinki' WHERE `id`='18';
UPDATE `quizcake`.`answers` SET `name`='Varsovia' WHERE `id`='19';
UPDATE `quizcake`.`answers` SET `name`='Paris' WHERE `id`='20';


UPDATE `quizcake`.`answers` SET `name`='Mario Vargas Llosa' WHERE `id`='21';
UPDATE `quizcake`.`answers` SET `name`='Julio Verne' WHERE `id`='22';
UPDATE `quizcake`.`answers` SET `name`='Gabriel García Marquez' WHERE `id`='23';
UPDATE `quizcake`.`answers` SET `name`='Gabriela Mistral' WHERE `id`='24';
UPDATE `quizcake`.`answers` SET `name`='Pablo Neruda' WHERE `id`='25';

UPDATE `quizcake`.`answers` SET `name`='Nueva York' WHERE `id`='26';
UPDATE `quizcake`.`answers` SET `name`='Albani' WHERE `id`='27';
UPDATE `quizcake`.`answers` SET `name`='Vermont' WHERE `id`='28';
UPDATE `quizcake`.`answers` SET `name`='New Jersey' WHERE `id`='29';
UPDATE `quizcake`.`answers` SET `name`='Manhattan' WHERE `id`='30';


UPDATE `quizcake`.`answers` SET `name`='Bogotá' WHERE `id`='31';
UPDATE `quizcake`.`answers` SET `name`='Buenos Aires' WHERE `id`='32';
UPDATE `quizcake`.`answers` SET `name`='Santiago de Chile' WHERE `id`='33';
UPDATE `quizcake`.`answers` SET `name`='Ciudad de México' WHERE `id`='34';
UPDATE `quizcake`.`answers` SET `name`='Sao Paulo' WHERE `id`='35';

UPDATE `quizcake`.`answers` SET `name`='Junker' WHERE `id`='36';
UPDATE `quizcake`.`answers` SET `name`='Enola Gay' WHERE `id`='37';
UPDATE `quizcake`.`answers` SET `name`='Meredith' WHERE `id`='38';
UPDATE `quizcake`.`answers` SET `name`='Stucka' WHERE `id`='39';
UPDATE `quizcake`.`answers` SET `name`='Kamikaze' WHERE `id`='40';

UPDATE `quizcake`.`answers` SET `name`='Queshua' WHERE `id`='41';
UPDATE `quizcake`.`answers` SET `name`='Chibcha' WHERE `id`='42';
UPDATE `quizcake`.`answers` SET `name`='Guarani' WHERE `id`='43';
UPDATE `quizcake`.`answers` SET `name`='Español' WHERE `id`='44';
UPDATE `quizcake`.`answers` SET `name`='Mapudungun' WHERE `id`='45';

UPDATE `quizcake`.`answers` SET `name`='Puerto Rico' WHERE `id`='46';
UPDATE `quizcake`.`answers` SET `name`='Hawai' WHERE `id`='47';
UPDATE `quizcake`.`answers` SET `name`='Samoa' WHERE `id`='48';
UPDATE `quizcake`.`answers` SET `name`='Midway' WHERE `id`='49';
UPDATE `quizcake`.`answers` SET `name`='Cuba' WHERE `id`='50';

UPDATE `quizcake`.`answers` SET `name`='Hecho acontecido' WHERE `id`='51';
UPDATE `quizcake`.`answers` SET `name`='Lugar indeterminado' WHERE `id`='52';
UPDATE `quizcake`.`answers` SET `name`='Actuar de hecho' WHERE `id`='53';
UPDATE `quizcake`.`answers` SET `name`='En el acto' WHERE `id`='54';
UPDATE `quizcake`.`answers` SET `name`='Lugar actual' WHERE `id`='55';

UPDATE `quizcake`.`answers` SET `name`='Tierra' WHERE `id`='56';
UPDATE `quizcake`.`answers` SET `name`='Venus' WHERE `id`='57';
UPDATE `quizcake`.`answers` SET `name`='Saturno' WHERE `id`='58';
UPDATE `quizcake`.`answers` SET `name`='Mercurio' WHERE `id`='59';
UPDATE `quizcake`.`answers` SET `name`='Neptuno' WHERE `id`='60';

