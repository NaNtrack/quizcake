INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (1, 'Pregunta 1', 1, 3, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (1, 'Pregunta 2', 2, 1, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (1, 'Pregunta 3', 3, 4, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (1, 'Pregunta 4', 4, 1, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (2, 'Pregunta 1', 1, 2, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (2, 'Pregunta 2', 2, 5, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (2, 'Pregunta 3', 3, 2, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (2, 'Pregunta 4', 4, 4, 3, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (3, 'Pregunta 1', 1, 5, 5, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (3, 'Pregunta 2', 2, 5, 5, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (3, 'Pregunta 3', 3, 3, 5, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3');
INSERT INTO `quizcake`.`questions` (`quiz_id`, `name`, `order`, `right_answer`, `points`, `url`) VALUES (3, 'Pregunta 4', 4, 2, 5, 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4');


UPDATE `quizcake`.`questions` SET `name`='Quién fue el primer presidente de Estados Unidos?', `right_answer`=4 WHERE `id`='1';
UPDATE `quizcake`.`questions` SET `name`='¿Cuál es la moneda de la India?', `right_answer`=2 WHERE `id`='2';
UPDATE `quizcake`.`questions` SET `name`='¿Qué pais de sudamérica esta rodeado por el océano Atlántico y oceano Pacífico?', `right_answer`=2 WHERE `id`='3';
UPDATE `quizcake`.`questions` SET `name`='¿Cuál es la capital de Suecia?', `right_answer`=2 WHERE `id`='4';

UPDATE `quizcake`.`questions` SET `name`='Qué escritor premio nobel escribió el libro Cien años de soledad?' WHERE `id`='5';
UPDATE `quizcake`.`questions` SET `name`='¿Cuál es la capital del estado de Nueva York?' WHERE `id`='6';
UPDATE `quizcake`.`questions` SET `name`='¿Qué capital latinoamericana es conocida como la Atenas Suramericana?' WHERE `id`='7';
UPDATE `quizcake`.`questions` SET `name`='¿Cómo se llamó el avión que lanzó la bomba atómica sobre Japón?' WHERE `id`='8';
UPDATE `quizcake`.`questions` SET `right_answer`=3 WHERE `id`='5';
UPDATE `quizcake`.`questions` SET `right_answer`=2 WHERE `id`='6';
UPDATE `quizcake`.`questions` SET `right_answer`=1 WHERE `id`='7';
UPDATE `quizcake`.`questions` SET `right_answer`=2 WHERE `id`='8';

UPDATE `quizcake`.`questions` SET `name`='Cuál es el dialecto originario de Paraguay?', `right_answer`=3 WHERE `id`='9';
UPDATE `quizcake`.`questions` SET `name`='¿Cuál es la principal isla de Estados Unidos?', `right_answer`=2 WHERE `id`='10';
UPDATE `quizcake`.`questions` SET `name`='¿Qué significado se le da en el castellano a la locución latina ipso facto?', `right_answer`=4 WHERE `id`='11';
UPDATE `quizcake`.`questions` SET `name`='¿Cuál de los siguientes planetas es el mas cercano al Sol?', `right_answer`=4 WHERE `id`='12';



update quizcake.questions set url = 'http://www.quizcake.com/files/1.png' 
                        where url = 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/1';

update quizcake.questions set url = 'http://www.quizcake.com/files/2.png' 
                        where url = 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/2';

update quizcake.questions set url = 'http://www.quizcake.com/files/3.png' 
                        where url = 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/3';

update quizcake.questions set url = 'http://www.quizcake.com/files/4.png' 
                        where url = 'http://sixwish.jp/AR/Marker/newidMark/model/2/id/4';