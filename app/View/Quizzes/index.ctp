<style>
	img.code{
		float: left;
		margin-right: 12px;
	}
	.question table{
		float:left;
		margin-left:3px;
		margin-top:-1px;
	}
	.question table td{
		padding-bottom: 11px;
	}
</style>
<h2>This is the test</h2>

<p>This is the question that we are going to make</p>
<div class="question">
	<img class="code" src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/students/code/4" width="60" height="60">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/img/circle.png" width="15" height="15" /> </td>
			<td>Respuesta 1</td>
		</tr>
		<tr>
			<td><img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/img/circle.png" width="15" height="15" /> </td>
			<td>Respuesta 2</td>
		</tr>
		<tr>
			<td><img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/img/circle.png" width="15" height="15" /> </td>
			<td>Respuesta 3</td>
		</tr>
		<tr>
			<td><img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/img/circle.png" width="15" height="15" /> </td>
			<td>Respuesta 4</td>
		</tr>
		<tr>
			<td><img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/img/circle.png" width="15" height="15" /> </td>
			<td>Respuesta 5</td>
		</tr>
	</table>
</div>


<?php if (false): ?>
<div class="quizzes index">
	<h2><?php echo __('Quizzes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('teacher_id'); ?></th>
			<th><?php echo $this->Paginator->sort('class'); ?></th>
			<th><?php echo $this->Paginator->sort('course'); ?></th>
			<th><?php echo $this->Paginator->sort('test_number'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($quizzes as $quiz): ?>
	<tr>
		<td><?php echo h($quiz['Quiz']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($quiz['Teacher']['name'], array('controller' => 'teachers', 'action' => 'view', $quiz['Teacher']['id'])); ?>
		</td>
		<td><?php echo h($quiz['Quiz']['class']); ?>&nbsp;</td>
		<td><?php echo h($quiz['Quiz']['course']); ?>&nbsp;</td>
		<td><?php echo h($quiz['Quiz']['test_number']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $quiz['Quiz']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $quiz['Quiz']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $quiz['Quiz']['id']), null, __('Are you sure you want to delete # %s?', $quiz['Quiz']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Quiz'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Teachers'), array('controller' => 'teachers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Teacher'), array('controller' => 'teachers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php endif; ?>