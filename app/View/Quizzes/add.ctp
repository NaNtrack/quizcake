<!-- New test -->
<div class="content-block">
	<h4>New Quiz</h4>
	<!-- content-container -->
	<div class="content-container">
		<?php echo $this->Form->create('Quiz'); ?>
		<!-- form-container -->
		<div class="form-container">
			<!-- start form -->
				<?php echo $this->Form->hidden('teacher_id', array(
					'value' => 2
				)); ?>
                <fieldset>
					<legend>Please select your course and class </legend>
					<!-- start row -->
					<div class="table-row span12 clearfix">
						<!-- nested row -->
						<div class="row-fluid clearfix">
							<!-- start column -->
							<div class="span4">
								<label>Quiz number</label>
								<?php 
								echo $this->Form->input('test_number', array(
									'div' => false,
									'label' => false
								)); 
								?>
							</div><!-- end column -->
							<!-- start column -->
							<div class="span4">
								<label>Course</label>
								<?php 
								echo $this->Form->input('course', array(
									'div' => false,
									'label' => false
								)); 
								?>
							</div><!-- end column -->
							<!-- start column -->
							<div class="span4">
								<label>Class</label>
								<?php 
								echo $this->Form->input('class', array(
									'div' => false,
									'label' => false
								)); 
								?>
							</div><!-- end column -->
						</div><!-- end nested row -->
					</div><!-- end table row -->
					<div class="form-buttons row-fluid claerfix">
						<?php echo $this->Form->submit('Save and continue', array(
							'class' => 'btn',
							'div' => false
						)); ?>
					</div>
                </fieldset>	
		</div><!-- end form-container -->
		<?php echo $this->Form->end(); ?>
	</div><!-- end content-conatiner -->
</div><!-- end New Test -->