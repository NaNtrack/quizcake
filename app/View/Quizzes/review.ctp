<script type="text/javascript" src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/flash/swfobject.js"></script>
<script type="text/javascript">
	// For version detection, set to min. required Flash Player version, or 0 (or 0.0.0), for no version detection. 
	var swfVersionStr = "11.2.0";
	// To use express install, set to playerProductInstall.swf, otherwise the empty string. 
	var xiSwfUrlStr = "http://<?php echo $_SERVER['HTTP_HOST']; ?>/flash/playerProductInstall.swf";
	var flashvars = {};
	var params = {};
	params.quality = "high";
	params.bgcolor = "#ffffff";
	params.allowscriptaccess = "sameDomain";
	params.allowfullscreen = "true";
	params.wmode = "direct";
	var attributes = {};
	attributes.id = "quizcake";
	attributes.name = "quizcake";
	attributes.align = "middle";
	swfobject.embedSWF(
	"http://<?php echo $_SERVER['HTTP_HOST']; ?>/flash/quizcake.swf", "flashContent", 
	"500", "375", 
	swfVersionStr, xiSwfUrlStr, 
	flashvars, params, attributes);
	// JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
	swfobject.createCSS("#flashContent", "display:block;text-align:left;");
</script>

<noscript>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="640" height="480" id="quizcake">
	<param name="movie" value="http://<?php echo $_SERVER['HTTP_HOST']; ?>/flash/quizcake.swf" />
	<param name="quality" value="high" />
	<param name="bgcolor" value="#ffffff" />
	<param name="allowScriptAccess" value="sameDomain" />
	<param name="allowFullScreen" value="true" />
	<param name="wmode" value="direct" />
	<!--[if !IE]>-->
	<object type="application/x-shockwave-flash" data="http://<?php echo $_SERVER['HTTP_HOST']; ?>/flash/quizcake.swf" width="640" height="480">
		<param name="quality" value="high" />
		<param name="bgcolor" value="#ffffff" />
		<param name="allowScriptAccess" value="sameDomain" />
		<param name="allowFullScreen" value="true" />
		<param name="wmode" value="direct" />
		<!--<![endif]-->
		<!--[if gte IE 6]>-->
		<p> 
			Either scripts and active content are not permitted to run or Adobe Flash Player version
			11.2.0 or greater is not installed.
		</p>
		<!--<![endif]-->
		<a href="http://www.adobe.com/go/getflashplayer">
			<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
		</a>
		<!--[if !IE]>-->
	</object>
	<!--<![endif]-->
</object>
</noscript>  

<script type="text/javascript">
<?php
$qs = array();
foreach ($questions as $q) {
	$qs[] = array(
		'o' => $q['Question']['order'],
		'r' => $q['Question']['right_answer']
	);
}
?>
		var qid = <?php echo $quiz['Quiz']['id']; ?>;
		var questions = <?php echo json_encode($qs) ?>;
		var currentStudent = 0;
		var cleaning = false;
		function newStudent(student_id){
			$('.student-current').removeClass('student-current').addClass('student-ok');
			$('#student_'+student_id).addClass('student-current');
			currentStudent = student_id;
			$('#questions_lists li').each(function(i,k) {
				$(this)
				.removeClass('marked')
				.removeClass('correct')
				.removeClass('incorrect')
				.html('<span>0'+(i+1)+'</span> Pending');
			});
		}
	
		function detectedAnswer(question_id, answer_id){
			//alert("Question: " + question_id + " -> " + answer_id);
			$('#question_'+question_id).addClass('marked');
			for(i in questions) {
				if (questions[i].o == question_id) {
					if (questions[i].r == answer_id+1) {
						$('#question_'+question_id).addClass('correct').html('<span>0'+question_id+'</span> Correct');
					} else {
						$('#question_'+question_id).addClass('incorrect').html('<span>0'+question_id+'</span> Incorrect');
					}
					break;
				}
			}
			if (currentStudent != 0) {
				$.ajax({
					type: 'POST',
					url: '/answer_students/add', 
					data: { s:currentStudent, q:qid, o:question_id, a: answer_id+1 }, 
					dataType: 'json',
					success: function(data){}
				});
			}
			console.log('Question detected: ' + question_id);
		}
</script>
<div class="row-fluid">
	<!--
	<li class="marked correct"><span>01</span> Correct</li>
	<li class="marked incorrect"><span>02</span> Incorrect (A)</li>
	<li><span>03</span>Pending</li>
	-->
	<!-- start row -->
	<div class="span3">
		<div class="students">
			<h3>Students</h3>

			<ul id="student_lists">
				<?php foreach ($students as $student) : ?>
					<li id="student_<?php echo $student['Student']['id']; ?>"><?php echo $student['Student']['name']; ?></li>
				<?php endforeach; ?>
				<!--
				<li class="student-ok">Perry Platypus <span>100%</span></li>
				<li class="student-current">Alumno 4</li>
				-->
			</ul>
		</div><!-- end alumnos -->
	</div><!-- end col1-->
	<div class="span3">
		<div class="feed">
			<h3>Questions</h3>
			<ul id="questions_lists">
				<?php foreach ($questions as $question): ?>
					<li id="question_<?php echo $question['Question']['order']; ?>"><span>0<?php echo $question['Question']['order']; ?></span>Pending</li>
				<?php endforeach; ?>
				<!--
				<li class="marked correct"><span>01</span> Correct</li>
				<li class="marked incorrect"><span>02</span> Incorrect (A)</li>
				<li><span>03</span>Pending</li>
				-->
			</ul>
		</div><!-- end feed -->
	</div><!-- end col2 -->
	<div class="span6" id="flashContent">
		<p>
			To view this page ensure that Adobe Flash Player version 
			11.2.0 or greater is installed. 
		</p>
		<script type="text/javascript"> 
			var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://"); 
			document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
				+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
		</script> 
	</div><!-- end col3 -->
</div><!-- end row -->