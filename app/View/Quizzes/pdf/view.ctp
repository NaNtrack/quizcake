<?php
$letters = array(
	0 => 'A',
	1 => 'B',
	2 => 'C',
	3 => 'D',
	4 => 'E'
);
?>
<?php foreach($students as $student) : ?>
	<h2><?php echo $quiz['Quiz']['course'] ?> / <?php echo $quiz['Quiz']['class'] ?> / Quiz N&deg;<?php echo $quiz['Quiz']['test_number'] ?></h2>
	<img style="vertical-align:top;float:left;" src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/students/code/<?php echo $student['Student']['id']; ?>"  width="60" height="60"/>
	<p style="float:left;margin-left: 30px;">Student: <?php echo $student['Student']['name']; ?> </p>
	<div class="clear">&nbsp;</div>
	<?php foreach ($quiz['Question'] as $question) : ?>
		<p><?php echo $question['order']; ?>.- <?php echo $question['name']; ?></p>
		<div class="question">
			<img class="code" src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/questions/code/<?php echo $question['id'] ?>" width="60" height="60">
			<table>
				<?php $i = 0; ?>
				<?php foreach($question['Answer'] as $answer) : ?>
				<tr>
					<td class="circle"><img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/img/circle.png" width="15" height="15" /> </td>
					<td><?php echo $letters[$i++]; ?>.- <?php echo $answer['name']; ?></td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
		<div class="clear">&nbsp;</div>
	<?php endforeach; ?>
<?php endforeach; ?>