<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>QuizCake Alpha</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Pablo Lobos">

		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/css/style.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script type="text/javascript" src="/js/script.js"></script>

		<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

	</head>

	<body>
		<div class="container clearfix">
			<!-- Row header -->
			<div class="row-fluid">
				<header class="main-header">
					<a style="display:block;" href="/"class="hide-text logo-int">QuizCake</a>
				</header>
			</div><!-- end row-fluid -->

			<!-- Teacher info -->
			<div class="row-fluid teacher-info clearfix">
				<header>

					<!-- Teacher ID -->
					<div class="teacher-id span4 clearfix">
						<img src="http://placehold.it/50x50/fff">
						<h3><?php echo $teacher['Teacher']['name']; ?></h3>
					</div><!-- end teacher-ID -->

					<!-- Teacher actions -->
					<div class="teacher-actions span8">
						<div class="btn-group">
							<?php echo $this->Html->link('My Quizzes', array('controller' => 'teachers', 'action' => 'view', 2), array('class' => 'btn')); ?>
							<?php echo $this->Html->link('New Quiz', array('controller' => 'quizzes', 'action' => 'add'), array('class' => 'btn')); ?>
						</div>
					</div><!-- end teacher actions -->

				</header>
			</div><!-- teacher info end -->

			<!-- Main content -->
			<div class="row-fluid main-content clearfix new-row">
				<?php echo $content_for_layout; ?>
			</div><!-- end main-content -->
		</div> <!-- /container -->
		<script src="/js/bootstrap.min.js"></script>
	</body>
</html>