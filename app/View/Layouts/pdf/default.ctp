<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $title_for_layout; ?></title>
	<style type="text/css">
	img.code{
		float: left;
		margin-right: 12px;
	}
	.question table{
		float:left;
		margin-left:0px;
		margin-top:-4px;
		width:500px;
	}
	.question table td{
		padding-right : 10px;
		height:25px;
		max-height:25px;
		font-size:11pt;
		line-height:13px;
		vertical-align:top;
	}
	.circle{
		width:15px;
		text-align:left;
	}
	.clear{
		clear:both;
	}
	</style>
</head>
<body>
	<?php echo $content_for_layout; ?>
</body>
</html>