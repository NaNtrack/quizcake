<div class="answerStudents view">
<h2><?php  echo __('Answer Student'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($answerStudent['AnswerStudent']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Student'); ?></dt>
		<dd>
			<?php echo $this->Html->link($answerStudent['Student']['name'], array('controller' => 'students', 'action' => 'view', $answerStudent['Student']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($answerStudent['Answer']['name'], array('controller' => 'answers', 'action' => 'view', $answerStudent['Answer']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Answer Student'), array('action' => 'edit', $answerStudent['AnswerStudent']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Answer Student'), array('action' => 'delete', $answerStudent['AnswerStudent']['id']), null, __('Are you sure you want to delete # %s?', $answerStudent['AnswerStudent']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Answer Students'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer Student'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Students'), array('controller' => 'students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Student'), array('controller' => 'students', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
