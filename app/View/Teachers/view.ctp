<!-- New test -->
<div class="content-block">
	<!-- content-container -->
	<div class="content-container">
		<h2 id="quiz-name">My Quizzes</h2><!-- http://www.appelsiini.net/projects/jeditable -->

		<!-- form-container -->
		<div class="form-container">
			<?php $i=0; ?>
			<?php foreach($quizzes as $quiz) : ?>
			<!-- start item-Quiz -->
			<div class="item-pregunta quiz-list-item clearfix">
				<div class="pregunta-header clearfix">
					<div class="pregunta-id"><?php echo ++$i?></div>
					<div id="question-name" class="pregunta-title edit editable-text">
						<?php echo $quiz['Quiz']['course'] ?> / <?php echo $quiz['Quiz']['class'] ?> / Quiz N&deg;<?php echo $quiz['Quiz']['test_number'] ?>
					</div><!-- end name -->

					<!-- Actions -->
					<div class="btn-group quiz-actions">
						<?php echo $this->Html->link('Edit', array('controller' => 'questions', 'action' => 'add', $quiz['Quiz']['id']), array('class' => 'btn')); ?>
						<?php echo $this->Html->link('Print', array('controller' => 'quizzes', 'action' => 'view', $quiz['Quiz']['id'].'.pdf'), array('class' => 'btn', 'target' => '_blank')); ?>
						<?php echo $this->Html->link('Mark', array('controller' => 'quizzes', 'action' => 'review', $quiz['Quiz']['id']), array('class' => 'btn')); ?>
					</div><!-- end actions -->
				</div><!-- end pregunta-header -->
			</div><!--end item-pregunta -->

			<?php endforeach; ?>
		</div><!-- end form-container -->

	</div><!-- end content-conatiner -->
</div><!-- end New Test -->