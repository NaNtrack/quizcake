<script type="text/javascript">
	function rightAnswer (q, r) {
		$('#right_answer_'+q).val(r);
	}
	
	var last_question = 0;
	
	function newQuestion() {
		$('#questions_container').append($('#clone').html());
		$('.pregunta-id').last().html(last_question);
		$('.right_answer_').last().attr('id','right_answer_'+last_question);
		$('.alternatives').last().find('li').each(function(i,k){
			$(this).find('input[type="radio"]').attr('onclick','return rightAnswer('+last_question+','+(i+1)+');')
		});
		last_question++;
		return false;
	}
</script>
<div id="clone" style="display:none;">
	<div class="item-pregunta">
		<div class="pregunta-header clearfix">
			<input type="hidden" name="data[Question][id][]" value="">
			<div class="pregunta-id">&nbsp;</div>
			<div id="question-name" class="pregunta-title">
				<input type="text" class="span12" name="data[Question][name][]" value="" placeholder="Please enter the question here..." />
				<input type="hidden" class="right_answer_" name="data[Question][right_answer][]" value="">
			</div>
		</div><!-- end pregunta-header -->
		<div class="alternatives clearfix">
			<h4>Alternatives</h4>
			<ul>
				<?php for ($i = 1 ; $i <= 5 ; $i++) : ?>
				<li>
					<input type="hidden" name="data[Answer][question_id][]" value="" />
					<input type="hidden" name="data[Answer][id][]" value="" />
					<input type="radio" name="data[selected][]" onclick="return rightAnswer(0,<?php echo $i; ?>);" />
					<button class="delete hide-text" onclick="deleteAlternative(0,<?php echo $i; ?>);">Delete alternative</button>
					<input type="text" name="data[Answer][name][]" value="" placeholder="Type the answer here" />
				</li>
				<?php endfor; ?>
			</ul>
		</div>
	</div><!--end item-pregunta -->
</div>

<!-- New test -->
<div class="content-block">
	<h4>Questions</h4>
	<?php echo $this->Form->create('Question'); ?>
	<input type="hidden" name="quiz_id" value="<?php echo $quiz['Quiz']['id']; ?>" />
	<!-- content-container -->
	<div class="content-container">
		<h2 id="quiz-name"><?php echo $quiz['Quiz']['course'] ?> / <?php echo $quiz['Quiz']['class'] ?> / Quiz N&deg;<?php echo $quiz['Quiz']['test_number'] ?></h2>
		<!-- form-container -->
		<div class="form-container">
			<legend>Please make some questions</legend>
			<?php $last_question = 0; ?>
			<div id="questions_container">
				<?php foreach ($questions as $question) : ?>
				<?php $last_question = $question['Question']['order']; ?>
				<!-- start item-pregunta -->
				<div class="item-pregunta">
					<div class="pregunta-header clearfix">
						<?php echo $this->Form->hidden('id][', array(
							'value' => $question['Question']['id']
						)); ?>
						<div class="pregunta-id"><?php echo $question['Question']['order']; ?></div>
						<div id="question-name" class="pregunta-title">
							<input type="text" class="span12" name="data[Question][name][]" value="<?php echo h($question['Question']['name']); ?>" placeholder="Please enter the question here..." />

						</div>
						<?php echo $this->Form->hidden('right_answer][', array(
							'value' => $question['Question']['right_answer'],
							'id' => 'right_answer_'.$question['Question']['id']
						)); ?>
					</div><!-- end pregunta-header -->
					<div class="alternatives clearfix">
						<h4>Alternatives</h4>
						<ul>
							<?php $alt = 0; ?>
							<?php foreach ($question['Answer'] as $answer) : ?>
							<?php $alt++; ?>
							<li>
								<input type="hidden" name="data[Answer][question_id][]" value="<?php echo $question['Question']['id']; ?>" />
								<input type="hidden" name="data[Answer][id][]" value="<?php echo $answer['id']; ?>" />
								<input type="radio" name="data[selected][<?php echo $question['Question']['id'] ?>]" onclick="return rightAnswer(<?php echo $question['Question']['id'] ?>,<?php echo $alt; ?>);" <?php if($alt == $question['Question']['right_answer']) : ?>checked="checked"<?php endif; ?>/>
								<button class="delete hide-text" onclick="deleteAlternative(<?php echo $answer['id']; ?>);">Delete alternative</button>
								<input type="text" name="data[Answer][name][]" value="<?php echo h($answer['name']); ?>" />
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div><!--end item-pregunta -->
				<?php endforeach; ?>
				<script>
					last_question = <?php echo ++$last_question; ?>
				</script>
			</div>
			<br/>
			<button class="add" onclick="return newQuestion();">Add question</button>
			<div class="form-buttons row-fluid claerfix">
				<button type="submit" class="btn">Save Questions and continue</button>
			</div>
		</div><!-- end form-container -->

	</div><!-- end content-conatiner -->
</div><!-- end New Test -->