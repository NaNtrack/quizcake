<div class="answers view">
<h2><?php  echo __('Answer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($answer['Answer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($answer['Question']['name'], array('controller' => 'questions', 'action' => 'view', $answer['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($answer['Answer']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Answer'), array('action' => 'edit', $answer['Answer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Answer'), array('action' => 'delete', $answer['Answer']['id']), null, __('Are you sure you want to delete # %s?', $answer['Answer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Answers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Answer Students'), array('controller' => 'answer_students', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Answer Student'), array('controller' => 'answer_students', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Answer Students'); ?></h3>
	<?php if (!empty($answer['AnswerStudent'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Student Id'); ?></th>
		<th><?php echo __('Answer Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($answer['AnswerStudent'] as $answerStudent): ?>
		<tr>
			<td><?php echo $answerStudent['id']; ?></td>
			<td><?php echo $answerStudent['student_id']; ?></td>
			<td><?php echo $answerStudent['answer_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'answer_students', 'action' => 'view', $answerStudent['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'answer_students', 'action' => 'edit', $answerStudent['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'answer_students', 'action' => 'delete', $answerStudent['id']), null, __('Are you sure you want to delete # %s?', $answerStudent['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Answer Student'), array('controller' => 'answer_students', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
