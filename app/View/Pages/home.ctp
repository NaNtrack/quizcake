<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>QuizCake Alpha</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Pablo Lobos">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

  </head>

  <body class="home">
    <div class="container clearfix">
      <header class="home-header">
        <h1 class="hide-text logo">QuizCake</h1>
      </header>
      <!-- feature -->
      <div class="feature">
        <img src="img/home-img.jpg">
        <p class="statement">Light speed Quiz marking, creation and administration. <?php echo $this->Html->link('Make and mark your first Quiz', array('controller' => 'quizzes', 'action' => 'add'), array('class' => 'button-call')); ?></p>

      </div><!-- end feature -->

      <div class="row-fluid">
        <div class="span4">
          <div class="imgcont-left">
            <img src="img/crea.png">
          </div>
          <div class="info-container info-left">
            <h3>Create</h3>
          </div>
          <div class="info-lista-left">
            <ul>
              <li>Create custom quizzes online!</li>
              <li>Review, edit and print your quizzes</li>
              <li>Customizable for each student/class</li>
            </ul>
          </div>
        </div>

        <div class="span4">
          <div class="imgcont-center">
            <img src="img/id.png">
          </div>
          <div class="info-container info-center">
            <h3>Identify</h3>
          </div>
          <div class="info-lista-center">
            <ul>
              <li>Identify each quiz and student</li>
              <li>Acurate questions detection</li>
              <li>Acurate answer detection</li>
            </ul>
          </div>
        </div>

        <div class="span4">
          <div class="imgcont-right">
            <img src="img/server.png">
          </div>
          <div class="info-container info-right">
            <h3>Online Storage</h3>
          </div>
          <div class="info-lista-right">
            <ul>
              <li>Save your quizzes online</li>
              <li>Up to 100Mb of free storage</li>
              <li>Premium accounts starting at $9.99/mo</li>
            </ul>
          </div>
        </div>
       
      </div><!-- end row -->

    </div> <!-- /container -->
   
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
