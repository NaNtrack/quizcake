<?php
App::uses('Quiz', 'Model');

/**
 * Quiz Test Case
 *
 */
class QuizTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.quiz',
		'app.teacher',
		'app.question',
		'app.answer',
		'app.answer_student',
		'app.student'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Quiz = ClassRegistry::init('Quiz');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Quiz);

		parent::tearDown();
	}

}
