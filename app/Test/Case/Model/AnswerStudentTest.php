<?php
App::uses('AnswerStudent', 'Model');

/**
 * AnswerStudent Test Case
 *
 */
class AnswerStudentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.answer_student',
		'app.student',
		'app.answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AnswerStudent = ClassRegistry::init('AnswerStudent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AnswerStudent);

		parent::tearDown();
	}

}
