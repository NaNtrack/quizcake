<?php
App::uses('AppController', 'Controller');
/**
 * Quizzes Controller
 *
 * @property Quiz $Quiz
 */
class QuizzesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Quiz->recursive = 0;
		$this->set('quizzes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Quiz->id = $id;
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid quiz'));
		}
		$this->Quiz->Behaviors->attach('Containable');
		$this->Quiz->contain(array(
			'Question' => array(
				'Answer'
			)
		));
		$this->set('quiz', $this->Quiz->read(null, $id));
		
		//Load all the students
		$this->loadModel('Student');
		$this->set('students', $this->Student->find('all'));
		
		$params = array(
			'download' => false,
			'name' => "quiz_$id.pdf",
			'paperOrientation' => 'portrait',
			'paperSize' => 'legal'
		);
		$this->set($params);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Quiz->create();
			if ($this->Quiz->save($this->request->data)) {
				$this->Session->setFlash(__('The quiz has been saved'));
				$this->redirect(array('controller' => 'questions','action' => 'add', $this->Quiz->id));
			} else {
				$this->Session->setFlash(__('The quiz could not be saved. Please, try again.'));
			}
		}
		$teachers = $this->Quiz->Teacher->find('list');
		$this->set(compact('teachers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Quiz->id = $id;
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid quiz'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Quiz->save($this->request->data)) {
				$this->Session->setFlash(__('The quiz has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quiz could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Quiz->read(null, $id);
		}
		$teachers = $this->Quiz->Teacher->find('list');
		$this->set(compact('teachers'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Quiz->id = $id;
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid quiz'));
		}
		if ($this->Quiz->delete()) {
			$this->Session->setFlash(__('Quiz deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Quiz was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
	public function review ($id) {
		$this->Quiz->id = $id;
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid quiz'));
		}
		$this->Quiz->Behaviors->attach('Containable');
		$this->Quiz->contain(array(
			'Question' => array(
				'Answer'
			)
		));
		$this->set('quiz', $this->Quiz->read(null, $id));
		
		//Load all the students
		$this->loadModel('Student');
		$this->set('students', $this->Student->find('all'));
		
		//Load all the questions of this quiz
		$this->loadModel('Question');
		$this->set('questions', $this->Question->find('all', array(
			'conditions' => array('quiz_id' => $id)
		)));
	}
	
}
