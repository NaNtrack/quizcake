<?php
App::uses('AppController', 'Controller');
/**
 * Questions Controller
 *
 * @property Question $Question
 * @property Quiz $Quiz
 */
class QuestionsController extends AppController {
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Question->recursive = 0;
		$this->set('questions', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		$this->set('question', $this->Question->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id) {
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$questions = array();
			foreach ($data['Question']['id'] as $k => $qid) {
				$question = array();
				$question['Question'] = array(
					'id' => $qid?$qid:null,
					'name' => $data['Question']['name'][$k],
					'right_answer' => $data['Question']['right_answer'][$k],
					'order' => $k+1,
					'quiz_id' => $data['quiz_id']
				);
				$this->Question->create();
				$this->Question->save($question);
				if(!$qid) {
					$i = 0;
					foreach($data['Answer']['question_id'] as $k => $question_id) {
						if (!$question_id) {
							$data['Answer']['question_id'][$k] = $this->Question->id;
							$i++;
							if ($i >= 5)
								break;
						}
					}
				}
			}
			//CakeLog::info(print_r($data, true));
			$this->loadModel('Answer');
			foreach($data['Answer']['question_id'] as $k => $qid) {
				$answer = array();
				$answer['Answer'] = array(
					'id' => $data['Answer']['id'][$k],
					'question_id' => $qid,
					'name' => $data['Answer']['name'][$k]
				);
				$this->Answer->create();
				$this->Answer->save($answer);
			}
		}
		$this->loadModel('Quiz');
		$this->Quiz->id = $id;
		if (!$this->Quiz->exists()) {
			throw new NotFoundException(__('Invalid quiz'));
		}
		
		$this->set('quiz', $this->Quiz->read(null, $id));
		
		$questions = $this->Question->find('all', array(
			'conditions' => array('Quiz.id' => $id)
		));
		$this->set(compact('questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Question->save($this->request->data)) {
				$this->Session->setFlash(__('The question has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Question->read(null, $id);
		}
		$quizzes = $this->Question->Quiz->find('list');
		$this->set(compact('quizzes'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->Question->delete()) {
			$this->Session->setFlash(__('Question deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Question was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	
	public function code ($id) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		
		$question = $this->Question->read(null, $id);
		$code = $question['Question']['order'];
		$this->redirect('http://'.$_SERVER['HTTP_HOST'].'/files/'.$code.'.png');
	}
	
}
