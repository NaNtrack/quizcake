<?php
App::uses('AppController', 'Controller');
/**
 * AnswerStudents Controller
 *
 * @property AnswerStudent $AnswerStudent
 */
class AnswerStudentsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AnswerStudent->recursive = 0;
		$this->set('answerStudents', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AnswerStudent->id = $id;
		if (!$this->AnswerStudent->exists()) {
			throw new NotFoundException(__('Invalid answer student'));
		}
		$this->set('answerStudent', $this->AnswerStudent->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->layout = 'ajax';
			$this->set('response', json_encode(array(
				'response' => 'OK'
			)));
		}
		$students = $this->AnswerStudent->Student->find('list');
		$answers = $this->AnswerStudent->Answer->find('list');
		$this->set(compact('students', 'answers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->AnswerStudent->id = $id;
		if (!$this->AnswerStudent->exists()) {
			throw new NotFoundException(__('Invalid answer student'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AnswerStudent->save($this->request->data)) {
				$this->Session->setFlash(__('The answer student has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The answer student could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AnswerStudent->read(null, $id);
		}
		$students = $this->AnswerStudent->Student->find('list');
		$answers = $this->AnswerStudent->Answer->find('list');
		$this->set(compact('students', 'answers'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AnswerStudent->id = $id;
		if (!$this->AnswerStudent->exists()) {
			throw new NotFoundException(__('Invalid answer student'));
		}
		if ($this->AnswerStudent->delete()) {
			$this->Session->setFlash(__('Answer student deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Answer student was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
