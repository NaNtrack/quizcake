<?php
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => true,
		'host' => 'localhost',
		'login' => 'quizcake',
		'password' => 'quizcake',
		'database' => 'quizcake',
	);
}
